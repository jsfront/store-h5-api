import { Injectable } from '@nestjs/common'
import * as pino from 'pino'
import * as moment from 'moment'

const logger = pino({
  prettyPrint: true
})

class LogData {
  public message?: string
  public data?: any
}

@Injectable()
export class LoggerService {
  private log(logMethod: string, logData: LogData) {
    logData = logData || { message: '', data: {}}
    ;(logData as any).timeLocal = moment().format('YYYY-MM-DD HH:mm:ss.SSS')
    logger[logMethod](logData)
  }

  debug(logData: LogData) {
    this.log('debug', logData)
  }

  info(logData: LogData) {
    this.log('info', logData)
  }

  error(logData: LogData) {
    this.log('error', logData)
  }

  warn(logData: LogData) {
    this.log('warn', logData)
  }

  fatal(logData: LogData) {
    this.log('fatal', logData)
  }
}
