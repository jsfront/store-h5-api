import { TypeOrmModuleOptions } from '@nestjs/typeorm'

export const TypeOrmConfigModule: TypeOrmModuleOptions = {
  type: 'mysql',
  host: process.env.DATABASE_HOST,
  port: parseInt(process.env.DATABASE_PORT, 10),
  username: process.env.DATABASE_USER,
  password: process.env.DATABASE_PASSWORD,
  database: process.env.DATABASE_NAME,
  // entities: ['dist/**/*.entity{.ts,.js}'],
  entities: ['*/**/*.entity.js'],
  logging: true, // 开启所有数据库信息打印,
  logger: 'advanced-console',
  synchronize: process.env.NODE_ENV === 'development'
}
