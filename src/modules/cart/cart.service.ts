import { Injectable } from '@nestjs/common'
import { InjectRepository } from '@nestjs/typeorm'
import { Repository } from 'typeorm'
import { ProductEntity } from '../products/entities/product.entity'
import { CreateCartDto } from './dto/create-cart.dto'
import { UpdateCartDto } from './dto/update-cart.dto'
import { CartEntity } from './entities/cart.entity'

@Injectable()
export class CartService {
  constructor(
    @InjectRepository(CartEntity)
    private readonly repository: Repository<CartEntity>,
    @InjectRepository(ProductEntity)
    private readonly productRepository: Repository<ProductEntity>
  ) {
  }

  create(createCartDto: CreateCartDto) {
    return 'This action adds a new cart'
  }

  findAll() {
    return `This action returns all cart`
  }

  findOne(id: number) {
    return `This action returns a #${id} cart`
  }

  update(id: number, updateCartDto: UpdateCartDto) {
    return `This action updates a #${id} cart`
  }

  remove(id: number) {
    return `This action removes a #${id} cart`
  }
}
