import { Module } from '@nestjs/common'
import { MembersService } from './member.service'
import { MembersController } from './member.controller'
import { TypeOrmModule } from '@nestjs/typeorm'
import { MemberEntity } from './entities/member.entity'

@Module({
  imports: [TypeOrmModule.forFeature([MemberEntity])],
  controllers: [MembersController],
  providers: [MembersService]
})
export class MembersModule {}
