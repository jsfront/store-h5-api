import { Injectable } from '@nestjs/common'
import { InjectRepository } from '@nestjs/typeorm'
import { Repository } from 'typeorm'
// import { CreateMemberDto } from './dto/create-member.dto'
// import { UpdateMemberDto } from './dto/update-member.dto'
import { MemberEntity } from './entities/member.entity'

@Injectable()
export class MembersService {
  constructor(
    @InjectRepository(MemberEntity)
    private readonly membersRepository: Repository<MemberEntity>,
  ) {}

  // create(createMemberDto: CreateMemberDto) {
  //   return 'This action adds a new member'
  // }

  findAll() {
    return `This action returns all members`
  }

  async findOne(id: number) {
    console.log(id)
    return await this.membersRepository.findOneOrFail(id)
  }

  // update(id: number, updateMemberDto: UpdateMemberDto) {
  //   return `This action updates a #${id} member`
  // }

  remove(id: number) {
    return `This action removes a #${id} member`
  }
}
