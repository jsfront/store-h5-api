import { Entity, Column, PrimaryGeneratedColumn } from 'typeorm'

@Entity('members_info')
export class MemberEntity {
  @PrimaryGeneratedColumn({ comment: 'id' })
  id: number

  @Column({ comment: '登录名' })
  user_id: string

  @Column({ comment: '登录名' })
  order_id: string

  @Column({ comment: '昵称', select: false, nullable: true })
  real_name: string

  @Column({ comment: '昵称', select: false, nullable: true })
  nick_name: string
  
  @Column({ comment: '密码', })
  password: string
  
  @Column({ comment: '密保邮箱', })
  avatar: string
  
  @Column({ comment: '密保手机号', })
  gender: string
  
  @Column({ comment: '密保手机号', })
  birthday: string
  
  @Column({ comment: '密保手机号', })
  phone: string
  
  @Column({ comment: '密保手机号', })
  email: string
  
  @Column({ comment: '密保手机号', })
  user_point: string
  
  @Column({ comment: '密保手机号', })
  customer_level: string
  
  @Column({ comment: '密保手机号', })
  user_money: string
  
  @Column({ comment: '密保手机号', })
  identity_card_type: string
  
  @Column({ comment: '密保手机号', })
  identity_card_no: string
  
  @Column({ comment: '密保手机号', })
  store_id: string
  
  @Column({ comment: '创建时间', })
  create_time: string
  
  @Column({ comment: '修改时间', })
  update_time: string
  
}