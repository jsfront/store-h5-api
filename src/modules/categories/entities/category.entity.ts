import { Entity, Column, PrimaryGeneratedColumn } from 'typeorm'

@Entity('product_category')
export class CategoryEntity {
  @PrimaryGeneratedColumn({ comment: 'id' })
  id: number

  @Column({ comment: '分类名称' })
  category_name: string

  @Column({ comment: '分类编码' })
  category_code: string

  @Column({ comment: '分类描述' })
  category_desc: string

  @Column({ comment: '排序' })
  category_order: number

  @Column({ comment: '父分类ID' })
  parent_id: number

  @Column({ comment: '分类层级' })
  category_level: number

  @Column({ comment: '分类状态' })
  category_status: string

  @Column({ comment: '分类 icon' })
  icon_url: string

  @Column({ comment: '分类图片' })
  pic_url: string

  @Column({ comment: '创建时间' })
  create_time: Date

  @Column({ comment: '更新时间' })
  update_time: Date

  children: CategoryEntity[]
}
