import { Entity, Column, PrimaryGeneratedColumn } from 'typeorm'

@Entity('order_info')
export class OrderEntity {
  @PrimaryGeneratedColumn({ comment: 'id' })
  id: number

  @Column({ comment: '订单编号' })
  order_no: string

  @Column({ comment: '外部支付编号' })
  external_pay_no: string

  @Column({ comment: '订单状态(1:待付款 2:待发货 3:已发货 4:已完成 5:已关闭 6:售后中  7:退款中 8:订单关闭)' })
  status: string

  @Column({ comment: '维权状态 (1:未维权 2:维权中 3:维权结束)', })
  rights_protection_status: string

  @Column({ comment: '收货人姓名', })
  receiver_name: string
  
  @Column({ comment: '收货人手机号', })
  receiver_phone: string
  
  @Column({ comment: '收货人手机号后四位', })
  receiver_phone_last_four: string
  
  @Column({ comment: '发货记录id', })
  shipment_id: string
  
  @Column({ comment: '发货记录编号', })
  shipment_no: string
  
  @Column({ comment: '物流编号', })
  tacking_no: string
  
  @Column({ comment: '总数量', })
  total_qty: number
  
  @Column({ comment: '总数量', })
  total_receivable_amount: number
  
  @Column({ comment: '总应收金额 单位分', })
  total_favorable_amount: number
  
  @Column({ comment: '支付方式 (1: 支付宝 2: 微信 3:银联 4:其他)', })
  pay_method: string
  
  @Column({ comment: '订单加星数', })
  pay_starred: string
  
  @Column({ comment: '订单类型 (1: 普通订单 2: 代付订单 3:送礼订单 4:送礼社群版订单 5:心愿订单 6:扫码付款 7:酒店订单 8:维权订单 9:周期购订单 10:多人拼团订单 11:知识付费订单)', })
  type: string
  
  @Column({ comment: '配送方式(1:快递发货 2:上门自提 3:同城配送)', })
  delivery_method: string
  
  @Column({ comment: '订单来源 (1: 浏览器 2:支付宝 3:浏览器 4:商家自有app 5:微信小程序 6: 其他)', })
  source: string
  
  @Column({ comment: '下单会员id', })
  member_id: number
  
  @Column({ comment: '店铺id', })
  store_id: string
  
  @Column({ comment: '创建时间', })
  create_time: Date
  
  @Column({ comment: '更新时间', })
  update_time: Date
  
}
