import { Injectable } from '@nestjs/common'
import { InjectRepository } from '@nestjs/typeorm'
import { Repository } from 'typeorm'
import { CreateOrderDto } from './dto/create-order.dto'
import { UpdateOrderDto } from './dto/update-order.dto'
import { OrderEntity } from './entities/order.entity'

@Injectable()
export class OrderService {
  constructor(
    @InjectRepository(OrderEntity) private readonly repository: Repository<OrderEntity>,
  ) {}

  create(createOrderDto: CreateOrderDto) {
    return this.repository.save(createOrderDto)
  }

  findAll() {
    return this.repository.find()
  }

  findOne(id: number) {
    return this.repository.find({
      where: { id }
    })
  }

  update(id: number, updateOrderDto: UpdateOrderDto) {
    return this.repository.update({ id }, updateOrderDto)
  }

  remove(id: number) {
    return `This action removes a #${id} order`
  }
}
