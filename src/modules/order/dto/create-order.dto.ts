import { ApiProperty } from "@nestjs/swagger"

export class CreateOrderDto {
  @ApiProperty({ description: `订单编号`, })
  order_no: string

  @ApiProperty({ description: `外部支付编号`, })
  external_pay_no: string
}
