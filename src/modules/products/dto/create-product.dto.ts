import { ApiProperty } from '@nestjs/swagger'

export class CreateProductDto {
  @ApiProperty()
  product_no: string

  @ApiProperty()
  product_name: string
}
