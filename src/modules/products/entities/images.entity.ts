import { Entity, Column, PrimaryGeneratedColumn } from 'typeorm'

@Entity('product_pic_info')
export class ImagesEntity {
  @PrimaryGeneratedColumn({ comment: 'id' })
  id: number

  @Column({ comment: '商品 id' })
  product_id: string

  @Column({ comment: '图片描述' })
  pic_desc: string

  @Column({ comment: '图片URL' })
  pic_url: string

  @Column({ comment: '是否主图：0.非主图1.主图' })
  is_master: boolean

  @Column({ comment: '图片排序' })
  pic_order: number

  @Column({ comment: '图片是否有效：0无效 1有效' })
  pic_status: number

  @Column({ comment: '创建时间' })
  create_time: Date

  @Column({ comment: '更新时间' })
  update_time: Date
}
