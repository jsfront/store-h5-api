import { Entity, Column, PrimaryGeneratedColumn } from 'typeorm'

@Entity('product_info')
export class ProductEntity {
  @PrimaryGeneratedColumn({ comment: 'id' })
  id: number

  @Column({ comment: '商品 id' })
  product_id: string

  @Column({ comment: '商品编码' })
  product_no: string

  @Column({ comment: '商品名称' })
  product_name: string

  @Column({ comment: '商品价格' })
  price: string

  @Column({ comment: '上下架状态：0下架1上架' })
  publish_status: boolean

  @Column({ comment: '生产日期' })
  production_date: string

  @Column({ comment: '商品描述' })
  description: string

  @Column({ comment: '商品图片' })
  pic_url: string

  @Column({ comment: '扩展属性' })
  property: string

  @Column({ comment: '会员价' })
  member_price: number

  @Column({ comment: '库存' })
  stock: number

  @Column({ comment: '店铺id' })
  store_id: string

  @Column({ comment: '创建时间' })
  create_time: Date

  @Column({ comment: '更新时间' })
  update_time: Date
}

