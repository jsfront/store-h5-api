import { Module } from '@nestjs/common'
import { ProductsService } from './products.service'
import { ProductsController } from './products.controller'
import { TypeOrmModule } from '@nestjs/typeorm'
import { ProductEntity } from './entities/product.entity'
import { ImagesEntity } from './entities/images.entity'

@Module({
  imports: [
    TypeOrmModule.forFeature([ProductEntity]),
    TypeOrmModule.forFeature([ImagesEntity])
  ],
  controllers: [ProductsController],
  providers: [ProductsService]
})
export class ProductsModule {}
