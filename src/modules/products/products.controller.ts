import { Controller, Get, Post, Body, Param, Delete } from '@nestjs/common'
import { ProductsService } from './products.service'
import { CreateProductDto } from './dto/create-product.dto'
import { ApiOperation, ApiTags } from '@nestjs/swagger'
// import { UpdateProductDto } from './dto/update-product.dto'

@ApiTags('商品')
@Controller('products')
export class ProductsController {
  constructor(private readonly service: ProductsService) {}

  @Post()
  @ApiOperation({ summary: '新增商品' })
  create(@Body() createProductDto: CreateProductDto) {
    return this.service.create(createProductDto)
  }

  @Get()
  @ApiOperation({ summary: '获取全部商品' })
  findAll() {
    return this.service.findAll()
  }

  @Get(':id')
  @ApiOperation({ summary: '获取商品详情' })
  findOne(@Param('id') id: string) {
    return this.service.findOne(+id)
  }

  @Get('/category/:id')
  @ApiOperation({ summary: '根据分类id获取商品列表' })
  findOneByCategoryId(@Param('id') id: string) {
    return this.service.findOneByCategoryId(+id)
  }

  @Get('/imgs/:id')
  @ApiOperation({ summary: '获取商品轮播图' })
  findImages(@Param('id') id: string) {
    return this.service.findImages(+id)
  }

  // @Patch(':id')
  // update(@Param('id') id: string, @Body() updateProductDto: UpdateProductDto) {
  //   return this.service.update(+id, updateProductDto)
  // }

  @Delete(':id')
  remove(@Param('id') id: string) {
    return this.service.remove(+id)
  }
}

