import './config/dotenv'
import { NestFactory } from '@nestjs/core'
import { AppModule } from './app.module'
import { SwaggerModule, DocumentBuilder } from '@nestjs/swagger'
import { HttpExceptionFilter } from './shared/core/filters/HttpExceptionFilter'
import { ResponseInterceptor } from './shared/core/interceptors/response.interceptor'
import { LoggerService } from './common/logger.service'

async function bootstrap() {
  const app = await NestFactory.create(AppModule, { cors: true })
  const logger: LoggerService = app.get(LoggerService)

  app.setGlobalPrefix('api')

  logger.info({
    message: 'Starting Nest application...',
    data: {
      NODE_ENV: process.env.NODE_ENV,
      port: 4000
    }
  })

  // 全局注册错误的过滤器
  app.useGlobalFilters(new HttpExceptionFilter())
  app.useGlobalInterceptors(new ResponseInterceptor())

  const options = new DocumentBuilder().setTitle('商城-h5-api').setDescription('针对h5商城提供的api').setVersion('1.0').addTag('h5-api').build()
  const document = SwaggerModule.createDocument(app, options)

  SwaggerModule.setup('api_docs', app, document)

  await app.listen(3000)
}

bootstrap()
