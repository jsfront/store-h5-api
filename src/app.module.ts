import { Module } from '@nestjs/common'
import { TypeOrmModule } from '@nestjs/typeorm'
// import { ConfigModule } from '@nestjs/config'
import { AppController } from './app.controller'
import { AppService } from './app.service'
import { MemberEntity } from './modules/member/entities/member.entity'
import { TypeOrmConfigModule } from './config/typeorm.config'
import { MembersModule } from './modules/member/member.module'
import { ProductsModule } from './modules/products/products.module'
import { CategoriesModule } from './modules/categories/categories.module'
import { CommonModule } from './common/common.module'
import { OrderModule } from './modules/order/order.module'

@Module({
  imports: [
    TypeOrmModule.forRoot(TypeOrmConfigModule),
    TypeOrmModule.forFeature([MemberEntity]),
    CommonModule,
    CategoriesModule,
    ProductsModule,
    MembersModule,
    OrderModule
  ],
  controllers: [AppController],
  providers: [AppService]
})
export class AppModule {}
